import React, {Component} from 'react';
import {RouteWithSubRoutes} from '../utils/router-utils';
import routes from '../routes/index';
import '../styles/main.scss';


class AppContainer extends Component {
  render() {
    console.log('appcontainer',routes)
    return (
      <div>
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route}/>
        ))}
      </div>
    );
  }
}

export default AppContainer;

