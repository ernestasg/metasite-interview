import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import { NavLink, withRouter } from 'react-router-dom';
import DropdownMenu from '../dropdown-menu/dropdown-menu';
import { BUTTON_GROUP_ITEMS, DROPDOWN_ITEMS, USER_NAME } from '../../constants/header';

class Header extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
  };

  state = {
    showMenu: false,
  };

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.setState({ showMenu: false });
      document.removeEventListener('click', this.closeMenu);
    }
  }

  showMenu = (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.setState({ showMenu: true });
    document.addEventListener('click', this.closeMenu);
  };

  closeMenu = (e) => {
    if (this.dropdownMenu) {
      if (!this.dropdownMenu.contains(e.target)) {
        this.setState({ showMenu: false });
        document.removeEventListener('click', this.closeMenu);
      }
    }
  };
  onSearchChange = (e) => {
    console.log('search changed ----- ', e.target.value);
  };

  render() {
    const { showMenu } = this.state;
    return (
      <div className="header-container">
        <div className="header-logo">
          CONTACTIFY
        </div>
        <div className="header-content">
          <div className="button-group">
            {BUTTON_GROUP_ITEMS.map(item => (
              <NavLink
                key={item.label}
                exact
                to={item.path}
                className={item.classNames}
                activeClassName="button-active"
              >
                <div className="header-item-content">
                  {item.label.toUpperCase()}
                </div>
              </NavLink>
            ))}
          </div>
          <div className="search-box-container">
            <input onChange={this.onSearchChange} placeholder="Search" type="text" />
            <span className="fa fa-search" />
          </div>
        </div>
        <div className="header-dropdown" onClick={this.showMenu}>
          <div className="header-dropdown-content">
            <i className="fa fa-user" />
            <span className="user-name">
              {USER_NAME}
            </span>
            <i className="fa fa-caret-down" />
          </div>
          <div className="submenu-container" ref={el => this.dropdownMenu = el}>
            <CSSTransition
              in={showMenu}
              classNames="slide"
              timeout={100}
              unmountOnExit
            >
              <DropdownMenu items={DROPDOWN_ITEMS} />
            </CSSTransition>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Header);
