import React from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';

export default function Button(props) {
  const {
    type = 'default',
    to = null,
    className = '',
    submit = false,
    loading = false,
    children = null,
    ...restprops
  } = props;

  const btnprops = {
    ...restprops,
    className: classnames(className, 'button', `${type}-button`, { 'btn-loading': loading }),
    type: submit
      ? 'submit'
      : 'button',
  };

  if (to) {
    return (
      <Link to={to} {...btnprops}>
        {children}
      </Link>
    );
  }
  if (loading) {
    return (
      <button {...btnprops}>
        <CircularProgress size={14} />
      </button>
    );
  }

  return (
    <button {...btnprops}>
      {children}
    </button>
  );
}
