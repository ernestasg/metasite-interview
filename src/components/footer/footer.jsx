import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Footer extends Component {
  static propTypes = {};
  static defaultProps = {};

  render() {
    return (
      <div className="footer-wrapper">
        <div className="footer-copyright-section">
          <div className="flexbox-column">
            <a href="#">Dashboard</a>
            <a href="#">Contacts</a>
            <a href="#">Notifications</a>
          </div>
          <div className="copyright-container">
            © 2015 Contactify <a href="#">About</a> <a href="#">Privacy</a>
          </div>
        </div>
        <div className="footer-information-section">
          <div className="footer-information-item border-bottom space-between">
            <div className="footer-information-item-content-container">
              <div className="footer-information-item-icon">
                <i className="fas fa-cloud-upload-alt" />
              </div>
              <div className="flexbox-col">
                <div>
                  Last synced:
                </div>
                <div>
                  2015-06-02 14:33:10
                </div>
              </div>
            </div>
            <div className="information-item-link-container">
              <i className="fas fa-sync-alt" />
              <a href="#">
                Force sync
              </a>
            </div>
          </div>
          <div className="footer-information-item border-top">
            <div className="footer-information-item-content-container full-width">
              <div className="footer-information-item-icon bottom">
                <i className="fas fa-stethoscope" />
              </div>
              <div className="flexbox-col">
                <div>
                  Help desk and Resolution center available:
                </div>
                <div>
                  I-IV 8:00-18:00, V 8:00-16:45
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-links-section flexbox-column">
          <a href="#">Groups</a>
          <a href="#">Frequently contacted</a>
          <a href="#">Preferences</a>
          <a href="#">Log out</a>
        </div>
      </div>
    );
  }
}

export default Footer;
