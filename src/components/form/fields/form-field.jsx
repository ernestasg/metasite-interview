import React from 'react';
import classnames from 'classnames';

export default function FormField(props) {

    const {
        className = '',
        children = [],
        label = null,
        name = null,
        showError = true,
        meta: {
            touched,
            error = null
        }
    } = props;
    const hasError = touched && error;
    return (
        <div className={classnames('form-field', name && `form-field-${name}`, className, hasError && 'error')}>
            {label && <label htmlFor={name} className="field-label">{label}</label>}
            <div className="field-control">
                {children}
                {showError && hasError && <div className="error-msg">{error}</div>}
            </div>
        </div>
    );
}
