import React from 'react';
import FormField from '../form-field';

export default function SimpleSelectInput(props) {
  const {
    options,
    label,
    meta,
    input,
    ...restprops
  } = props;
  return (
    <FormField meta={meta}>
      <div className="styled-select">
        <select {...input} {...restprops}>
          <option value="" disabled selected>{label}</option>
          {options.map((opt, idx) => <option key={idx} value={opt.value}>{opt.label}</option>)}
        </select>
      </div>
    </FormField>
  );
}
