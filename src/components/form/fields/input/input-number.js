import React from 'react';
import FormField from '../form-field';

import TextField from 'material-ui/TextField';

export default function NumberInputField(props) {
    const {
        input,
        label,
        type,
        meta,
        showError,
        placeholder
    } = props;
    return (
        <FormField meta={meta} showError={showError}>
            <InputNumber {...input} placeholder={placeholder} label={label} type={type || 'number'} autoComplete="off"/>
        </FormField>

    );
}


class InputNumber extends React.Component {

    handleChange(evt) {
        this.props.onChange((evt.target.validity.valid) ? evt.target.value : this.props.value);
    }

    render() {
        return (
            <TextField
                id="number"
                label={this.props.label}
                value={this.props.value}
                onChange={this.handleChange.bind(this)}
                type="number"
                margin="normal"
            />
        );
    }
}


