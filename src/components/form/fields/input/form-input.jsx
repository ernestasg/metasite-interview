import React from 'react';
import PropTypes from 'prop-types';
import FormField from '../form-field';


export class SimpleInput extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
  };
  static defaultProps = {
    classes: {},
  };

  render() {
    const {
      input,
      label,
      type,
      meta,
      showError,
      disabled,
      placeholder,
      classes,
    } = this.props;
    return (
      <FormField meta={meta} showError={showError}>
        <input
          {...input}
          type={type}
          autoComplete="off"
          placeholder={placeholder}
          disabled={disabled}
        />
      </FormField>
    );
  }
}

export default SimpleInput;