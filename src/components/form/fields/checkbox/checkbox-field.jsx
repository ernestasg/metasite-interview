import React from 'react';

export default function CheckboxField(props) {
    const {input, label, type='checkbox', customFieldName} = props;
    return (
        <div className="form-field">
            <div className="checkbox-block">
                <input type={type} className="checkbox-item" id={customFieldName || input.name} {...input} checked={input.value} />
                <label htmlFor={customFieldName || input.name}>
                    <span>{label}</span>
                </label>
            </div>
        </div>

    )
}
