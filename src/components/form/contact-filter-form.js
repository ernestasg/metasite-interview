import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Field, reduxForm, getFormValues, change, untouch } from 'redux-form';
import { validateRequiredFields } from '../../utils/form';
import FormInput from './fields/input/form-input';
import SelectInput from './fields/select/select-field';
import CheckboxInput from './fields/checkbox/checkbox-field';
import Button from '../../components/buttons/button';
import contacts from '../../constants/contacts.json';

const uniqueCities = [];
contacts.forEach(contact => {
  !uniqueCities.includes(contact.city) && uniqueCities.push(contact.city);
});

const cityOptions = [];
uniqueCities.forEach(city => cityOptions.push({label: city, value: city}));

class ContactFilterForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
  }

  clearValue = (field) => {
    this.props.dispatch(change('contactFilter', field, null));
    this.props.dispatch(untouch('contactFilter', field));
  };

  render() {
    const { error, formValues } = this.props;
    return (
      <div className="contacts-header-form-container">
        <form onSubmit={this.props.handleSubmit} className="form">
          {error && <div className="global-error">{error}</div>}
          <div className="name-field-wrapper">
            <Field name="name" component={FormInput} placeholder={'Name'} type="text" />
          </div>
          <div className="city-field-wrapper">
            <Field name="city" component={SelectInput} label={'City'} options={cityOptions} />
            {formValues ?
              formValues.city ?
                <div className="clear-selection" onClick={() => this.clearValue('city')}>Clear Selection</div>
                : null
              : null}
          </div>
          <div className="checkbox-field-wrapper">
            <Field name="active" component={CheckboxInput} label={'Show active'} />
          </div>
          <div className="form-submit-container">
            <Button submit>FILTER</Button>
          </div>
        </form>
      </div>
    );
  }
}

function validate(values) {
  let errors = {};
  errors = validateRequiredFields(values, [], errors);

  return errors;
}

ContactFilterForm = reduxForm({
  form: 'contactFilter',
  validate,
})(ContactFilterForm);

const mapStateToProps = (state) => {
  return {
    filterForm: state.form.contactFilter,
    formValues: getFormValues('contactFilter')(state),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContactFilterForm);
// export default ContactFilterForm;