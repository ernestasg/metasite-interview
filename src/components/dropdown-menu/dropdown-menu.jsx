import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

class DropdownMenu extends Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
  };

  renderItems = (items) => {
    return items.map((item, idx) => {
      const shouldHaveBorder = idx < items.length - 1;
      if (!item.path) {
        return (
          <div
            onClick={item.func}
            className={`nav__submenu-item ${shouldHaveBorder ? 'nav__submenu-item-border-bottom' : ''}`}
            key={item.label}
          >
            {item.icon}
            {item.label}
          </div>
        );
      }
      return (
        <NavLink
          exact
          to={item.path}
          className={`nav__submenu-item ${shouldHaveBorder ? 'nav__submenu-item-border-bottom' : ''}`}
          activeClassName="nav__submenu-item nav__submenu-item-active"
          key={item.label}
        >
          {item.icon}
          {item.label}
        </NavLink>
      );
    });
  };

  render() {
    const { items } = this.props;
    return (
      <ul className="nav__submenu">
        {this.renderItems(items)}
      </ul>
    );
  }
}

export default DropdownMenu;
