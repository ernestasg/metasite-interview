import fetch from 'isomorphic-fetch';

function callApi (url, options) {
  return fetch(url, options).then((response) => {
    const contentType = response.headers.get('content-type')
    const isJson = contentType && contentType.indexOf('application/json') >= 0

    if (response.status >= 200 && response.status < 300) {
      return isJson ? Promise.resolve(response.json()) : Promise.resolve(response.text())
    }

    const error = new Error(response.statusText || response.status)
    if (isJson)
      return response.json().then((json) => {
        error.response = json
        throw error
      });

    error.response = {message: 'Unexpected Error'}
    throw error
  });
}

export function callGet (url) {
  return callApi(url, {
    method: 'GET'
  });
}

export function callPost (url, data) {
    return callApi(url, {
        method: 'POST',
        body: JSON.stringify(data)
    });
}

export function callUpdate (url, data) {
    return callApi(url, {
        method: 'UPDATE',
        body: JSON.stringify(data)
    });
}

export function callDelete (url) {
    return callApi(url, {
        method: 'DELETE'
    });
}