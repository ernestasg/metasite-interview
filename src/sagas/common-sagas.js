import {
  handleFormSubmit as handleFormSubmitAction,
  handleFormUpdate as handleFormUpdateAction,
} from '../utils/redux-form';
import { put, call } from 'redux-saga/effects';
import API from '../utils/api';

function* handleFormAction(url, action, actionType, submitAction) {
  try {
    const result = yield submitAction(url, action);
    yield put({ type: actionType.SUCCESS, payload: result });
  } catch (e) {
    yield put({ type: actionType.FAILURE, payload: e });
  }
}

export function* handleFormSubmit(url, action, actionType) {
  yield handleFormAction(url, action, actionType, handleFormSubmitAction);
}

export function* handleFormUpdate(url, action, actionType) {
  yield handleFormAction(url, action, actionType, handleFormUpdateAction);
}

export function* fetchItemSaga(url, actionType) {
  try {
    const result = yield call(API.callGet, url);
    yield put({ type: actionType.SUCCESS, payload: result });
  } catch (e) {
    yield put({ type: actionType.FAILURE, payload: e });
  }
}

export function* postItemSaga(url, data, actionType) {
  try {
    const result = yield call(API.callPost, url, data);
    yield put({ type: actionType.SUCCESS, payload: result });
  } catch (e) {
    yield put({ type: actionType.FAILURE, payload: e });
  }
}

export function* removeItemSaga(url, data, actionType) {
  try {
    yield call(API.callDelete, url);
    yield put({ type: actionType.SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: actionType.FAILURE, payload: e });
  }
}

export function* updateItemSaga(url, data, actionType) {
  try {
    yield call(API.callUpdate, url);
    yield put({ type: actionType.SUCCESS, payload: data });
  } catch (e) {
    yield put({ type: actionType.FAILURE, payload: e });
  }
}