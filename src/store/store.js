import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import RootSaga from './root-saga';
import combineReducers from './reducers';

const isProduction = process.env === 'production';
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export function configureStore(middlewares) {
  const sagaMiddleware = createSagaMiddleware();
  let middleware = [sagaMiddleware, thunk, ...middlewares];
  if (!isProduction) {
    middleware.push(createLogger());
  }

  return {
    ...createStore(combineReducers,
      composeEnhancers(applyMiddleware(...middleware))),
    runSaga: sagaMiddleware.run,
  };
}

export function buildStore(middlewares) {
  const store = configureStore(middlewares);
  store.runSaga(RootSaga);
  return store;
}
