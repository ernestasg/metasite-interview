import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import homeReducer from '../routes/home/modules/reducer';
import contactsReducer from '../routes/contacts/modules/reducer';

export default combineReducers({
  form: formReducer,
  router: routerReducer,

  home: homeReducer,
  contacts: contactsReducer,
});
