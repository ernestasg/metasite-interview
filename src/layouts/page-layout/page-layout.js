import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { RouteWithSubRoutes } from '../../utils/router-utils';
import { Switch, withRouter } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import Header from '../../components/header/header';

class CoreLayout extends React.Component {
  getLayoutClass = (pathname) => {
    let layoutClass = 'about-layout';
    const path = pathname.substring(1);
    if (path.length > 0) {
      layoutClass = `${path}-layout`;
    }
    return layoutClass;
  };

  render() {
    const { location } = this.props;
    return (
      <div className="container text-center">
        <div className={`page-layout ${this.getLayoutClass(location.pathname)}`}>
          <Header />
          <Switch>
            {this.props.routes.map((route, i) => (<RouteWithSubRoutes key={i} {...route} />))}
          </Switch>
        </div>
      </div>
    );
  }
}

CoreLayout.propTypes = {
  children: PropTypes.node,
};

export default withRouter(connect()(CoreLayout));
