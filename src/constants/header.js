import React from 'react';

export const BUTTON_GROUP_ITEMS = [
  {
    path: '/dashboard',
    label: 'Dashboard',
    classNames: 'group-button button-left',
  },
  {
    path: '/contacts',
    label: 'Contacts',
    classNames: 'group-button',
  },
  {
    path: '/notifications',
    label: 'Notifications',
    classNames: 'group-button button-right',
  },
];

export const DROPDOWN_ITEMS = [
  {
    path: '/groups',
    label: 'Groups',
    icon: <i className="fas fa-users" />,
  },
  {
    path: '/frequently_contacted',
    label: 'Frequently contacted',
    icon: <i className="fas fa-comments" />,
  },
  {
    path: '/preferences',
    label: 'Preferences',
    icon: <i className="fas fa-wrench" />,
  },
  {
    path: null,
    label: 'Log out',
    icon: <i className="fas fa-power-off" />,
    func: () => console.log('Logging out'),
  },
];

export const USER_NAME = 'Jorah Mormont';