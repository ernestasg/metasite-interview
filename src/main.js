import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import {Provider} from 'react-redux';
import {buildStore} from './store/store';
import App from './components/App';


import {ConnectedRouter, routerMiddleware} from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

const history = createHistory();
const middleware = routerMiddleware(history);


const MOUNT_NODE = document.getElementById('root');
const store = buildStore(middleware);

const renderRoot = Component => ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <AppContainer>
        <Component store={store}/>
      </AppContainer>
    </ConnectedRouter>
  </Provider>, MOUNT_NODE);

renderRoot(App);

if (module.hot) {
  module.hot.accept(() => {
    const reducers = require('./store/reducers').default;
    store.replaceReducer(reducers(store.asyncReducers));
    renderRoot(App);
  });
}

