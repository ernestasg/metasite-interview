import BaseReducer from '../../../reducers/base-reducer';
import types from '../../../actions/types';

class ApplicationsListReducer extends BaseReducer {
    constructor() {
        super();
        this.initialState = {

        };
        this.ACTION_HANDLERS = {
        };
    }

    deployApplicationRequest = (state) => {
        return {
            ...state,
            deployApplicationStatus: {
                loading: true
            }
        };
    };
}

export default new ApplicationsListReducer().handleActions;
