import CoreLayout from '../layouts/page-layout/page-layout';
import Home from './home';
import Contacts from './contacts';

export default [{
  path: '/',
  component: CoreLayout,
  routes: [
    Home,
    Contacts,
  ],
}];
