import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ContactsHeader from './components/contacts-header';
import { CONTACTS_TABLE_COLUMNS } from '../../constants/contacts';
import profile from '../../../public/images/userpic.jpg';
import * as contactsActions from './modules/actions';
import Footer from '../../components/footer/footer';

const CONTACTS_TABLE_ROW_COUNT = 8;

class ContactsViewContainer extends Component {

  state = {
    sortOrder: '',
  };

  selectContact = (contact) => {
    this.props.actions.selectContact(contact);
  };

  toggleSort = () => {
    this.setState(state => (
      { sortOrder: state.sortOrder === 'asc' ? 'desc' : 'asc' }
    ));
  };

  renderContactCard = (selectedContact) => {
    if (selectedContact) {
      return (
        <div className="contact-card-container">
          <img className="contact-card-image" src={profile} />
          <div className="contact-card-information">
            {Object.keys(selectedContact).map((key, idx) => {
              if (key !== 'id' && key !== 'active') {
                return (
                  <div
                    key={idx}
                    className="contact-card-item"
                  >
                    <div className="contact-card-item-label">
                      {`${key.substr(0, 1).toUpperCase() + key.substr(1, key.length - 1)}:`}
                    </div>
                    <div className={key === 'email' && 'contact-card-item-email'}>
                      {selectedContact[key]}
                    </div>
                  </div>
                );
              }
            })}
          </div>
        </div>
      );
    }
    return (
      <div className="contact-card-container justify-center">
        Select a contact from the table
      </div>
    );
  };

  checkSelectedContact = (selectedContact, itemId) => {
    if (selectedContact) {
      if (selectedContact.id === itemId) {
        return 'row-selected';
      }
    }
  };

  renderEmptyRows = (rowCount) => {
    const rows = [];
    for (let i = 0; i < rowCount; i++) {
      let idx = (8 - rowCount) + i;
      rows.push(<tr
        key={idx}
        className={`${idx === CONTACTS_TABLE_ROW_COUNT - 1 && 'row-no-border'}`}
      >
        <td className="contacts-table-col" />
        <td className="contacts-table-col" />
        <td className="contacts-table-col" />
        <td className="contacts-table-col" />
        <td className="contacts-table-col" />
        <td className="contacts-table-col" />
      </tr>);
    }
    return rows.map(row => row);
  };

  renderContacts = (selectedContact, contacts) => {
    const { sortOrder } = this.state;
    if (sortOrder === 'asc') {
      contacts.sort((a, b) => a.name > b.name);
    } else if (sortOrder === 'desc') {
      contacts.sort((a, b) => a.name < b.name);
    }
    let emptyRowCount = 0;
    if (contacts.length < CONTACTS_TABLE_ROW_COUNT) {
      emptyRowCount = CONTACTS_TABLE_ROW_COUNT - contacts.length;
    }

    return (
      <tbody>
      {contacts.map((contact, idx) => (
        <tr
          key={idx}
          className={
            `${idx === CONTACTS_TABLE_ROW_COUNT - 1 && 'row-no-border'}
                   ${this.checkSelectedContact(selectedContact, contact.id)}`
          }
          onClick={() => this.selectContact(contact)}
        >
          <td className="contacts-table-col col-name">
            <i className="far fa-eye" />
            {contact.name}
          </td>
          <td className="contacts-table-col">{contact.surname}</td>
          <td className="contacts-table-col">{contact.city}</td>
          <td className="contacts-table-col">{contact.email}</td>
          <td className="contacts-table-col col-phone">{contact.phone}</td>
          <td className="contacts-table-col col-options">
            <div className="icon-container">
              <i className="fas fa-pen" />
            </div>
            <div className="icon-container">
              <i className="fas fa-trash-alt" />
            </div>
          </td>
        </tr>
      ))}
      {this.renderEmptyRows(emptyRowCount, contacts)}
      </tbody>
    );
  };

  renderNameHead = (idx, col, order) => {
    return (
      <th
        key={idx}
        scope="col"
        className="contacts-table-col-head col-head-name"
      >
        <div>
          {idx === 0 && <i className="far fa-eye placeholder-icon" />}
          {col}
        </div>
        {order === 'asc' ?
          <i
            className="fas fa-arrow-down sort-icon"
            onClick={this.toggleSort}
          /> :
          <i
            className="fas fa-arrow-up sort-icon"
            onClick={this.toggleSort}
          />
        }
      </th>
    );
  };

  render() {
    console.log('contacts state--', this.state);
    const {
      selectedContact,
      actions,
      contacts,
    } = this.props;
    const {
      sortOrder,
    } = this.state;
    return (
      <div className="contacts-page-wrapper">
        <ContactsHeader filterContacts={actions.filterContacts} />
        <div className="contacts-content-container">

          {this.renderContactCard(selectedContact)}
          <table className="contacts-list-table">
            <thead>
            <tr>
              {CONTACTS_TABLE_COLUMNS.map((col, idx) => {
                if (idx === 0) {
                  return this.renderNameHead(idx, col, sortOrder);
                }
                return (
                  <th
                    key={idx}
                    scope="col"
                    className={`contacts-table-col-head col-head-${col.toLowerCase()}`}
                  >
                    {col}
                  </th>
                );
              })}
              <th scope="col" className="head-no-border contacts-table-col-head col-head-options" />
            </tr>
            </thead>
            {this.renderContacts(selectedContact, contacts)}
          </table>
        </div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedContact: state.contacts.selectedContact,
    contacts: state.contacts.contacts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    actions: bindActionCreators({
      ...contactsActions,
    }, dispatch),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContactsViewContainer);
