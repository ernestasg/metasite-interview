import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ContactFilterForm from '../../../components/form/contact-filter-form';
import Button from '../../../components/buttons/button';

class ContactsHeader extends Component {
  static propTypes = {
    filterContacts: PropTypes.func.isRequired,
  };
  static defaultProps = {};

  onSubmit = (params) => {
    this.props.filterContacts(params);
  };

  addNewContact = () => {
    console.log('Add new contact');
  };

  render() {
    return (
      <div className="contacts-header-wrapper">
        <ContactFilterForm onSubmit={this.onSubmit} />
        <Button
          className="add-new-contact-button"
          onClick={this.addNewContact}
        >
          ADD NEW CONTACT
        </Button>
      </div>
    );
  }
}

export default ContactsHeader;
