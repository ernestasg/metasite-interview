import types from '../../../actions/types';

export function selectContact(contact) {
  return { type: types.SELECT_CONTACT, payload: contact };
}

export function filterContacts(filter) {
  return { type: types.FILTER_CONTACTS, payload: filter };
}