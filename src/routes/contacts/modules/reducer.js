import BaseReducer from '../../../reducers/base-reducer';
import types from '../../../actions/types';
import contacts from '../../../constants/contacts.json';

const filterMap = {
  name: (item, filters) => item.name.toLowerCase().includes(filters.name.toLowerCase()),
  city: (item, filters) => item.city === filters.city || !filters.city,
  active: (item, filters) => item.active || !filters.active,
};

const filter = (filters) => {
  let filteredItems = [];
  const filterKeys = Object.keys(filters);
  if (filterKeys.length > 0) {
    filterKeys.forEach((filterKey, idx) => {
      if (idx > 0) {
        return filteredItems.forEach(contact => {
          return !filterMap[filterKey](contact, filters) ?
            filteredItems = filteredItems.filter((item) => item.id !== contact.id) : null;
        });
      }
      return contacts.forEach(contact => {
        return filterMap[filterKey](contact, filters) ? filteredItems.push(contact) : null;
      });
    });
  } else {
    filteredItems = contacts;
  }
  return filteredItems;
};

class ContactsReducer extends BaseReducer {
  constructor() {
    super();
    this.initialState = {
      selectedContact: null,
      contacts: contacts,
    };
    this.ACTION_HANDLERS = {
      [types.SELECT_CONTACT]: this.selectContact,
      [types.FILTER_CONTACTS]: this.filterContacts,
    };
  }

  selectContact = (state, action) => {
    return {
      ...state,
      selectedContact: action.payload,
    };
  };

  filterContacts = (state, action) => {
    return {
      ...state,
      contacts: filter(action.payload),
    };
  };
}

export default new ContactsReducer().handleActions;
