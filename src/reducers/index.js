import { combineReducers } from 'redux';
import {routerReducer} from 'react-router-redux';
import {reducer as formReducer} from 'redux-form';
import homeReducer from '../routes/home/home-container';

export default combineReducers({
  form: formReducer,
  router: routerReducer,

  home: homeReducer,
});
