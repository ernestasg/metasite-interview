const logger = require('../lib/logger')

logger.info('Starting server...');
const port = process.env.PORT ? process.env.PORT : 3000;
require('../../server/main').listen(port, () => {
  logger.success('Server is running at http://localhost:' + port);
});
